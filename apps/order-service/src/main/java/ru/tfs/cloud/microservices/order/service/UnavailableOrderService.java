package ru.tfs.cloud.microservices.order.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import ru.tfs.cloud.microservices.order.dto.OrderDto;
import ru.tfs.cloud.microservices.order.repository.OrdersRepository;

@Slf4j
@Service
@Profile("circuit-breaker")
public class UnavailableOrderService implements OrderService {

    private final AtomicInteger counter = new AtomicInteger();
    private final OrderService originalOrderService;

    public UnavailableOrderService() {
        this.originalOrderService = new OrderServiceImpl(new OrdersRepository());
    }

    @Override
    public List<OrderDto> getCustomerOrders(long customerId) {
        var attempt = counter.getAndIncrement();
        log.warn("get orders by customer id {}, attempt #{}", customerId, attempt);
        if (attempt % 3 == 0) {
            throw new RuntimeException("Service is interrupted");
        } else {
            return originalOrderService.getCustomerOrders(customerId);
        }
    }

}
