package ru.tfs.cloud.microservices.order.controller;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.tfs.cloud.microservices.order.dto.OrderDto;
import ru.tfs.cloud.microservices.order.service.OrderService;

@RestController
@RequestMapping("/api/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping("/{customerId}")
    public List<OrderDto> getCustomerById(@PathVariable long customerId) {
        return orderService.getCustomerOrders(customerId);
    }

    @GetMapping("/secure")
    public String secureOrderInfo() {
        return "Secure info: #####";
    }
}
