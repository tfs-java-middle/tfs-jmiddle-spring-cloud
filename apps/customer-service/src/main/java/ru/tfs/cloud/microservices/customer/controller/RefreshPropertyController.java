package ru.tfs.cloud.microservices.customer.controller;

import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/customer")
@RequiredArgsConstructor
@RefreshScope
public class RefreshPropertyController {

    @Value("${custom.application.name:#{null}}")
    private String appName;

    @PostConstruct
    public void refreshBean() {
        log.error("CustomersService: Context was refreshed. Current AppName {}", appName);
    }

    @GetMapping("/app-name")
    public ResponseEntity<String> getCurrentAppName() {
        return ResponseEntity.ok(appName);
    }
}
